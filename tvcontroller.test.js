describe('TV controller', () => {
  it('can be turn on', () => {
    const controller = new TVController()

    controller.switchOn()

    expect(controller.on).toBe(true)
  })
})